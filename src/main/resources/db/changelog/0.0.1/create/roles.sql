--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table roles
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE roles
(
    id         BIGINT NOT NULL PRIMARY KEY,
    authority  VARCHAR(100) UNIQUE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
)
