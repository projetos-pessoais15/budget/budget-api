--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table revenues
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE revenues
(
    id         UUID NOT NULL primary key,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    name       VARCHAR(50) UNIQUE,
    wage       NUMERIC(7, 2),
    date       DATE
)
