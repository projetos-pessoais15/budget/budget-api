package br.com.budget.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Getter
@Builder
public class ErrorResponse {

    @Setter
    private Instant timestamp;

    @Setter
    private int status;

    @Setter
    private String statusName;

    @Setter
    private String exception;

    @Setter
    private String message;

    @Setter
    private String path;

    private List<FieldError> fieldErrors;

}
