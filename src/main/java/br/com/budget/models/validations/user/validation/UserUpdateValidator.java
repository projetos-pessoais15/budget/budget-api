package br.com.budget.models.validations.user.validation;

import br.com.budget.models.FieldMessage;
import br.com.budget.models.dto.UserUpdateDTO;
import br.com.budget.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequiredArgsConstructor
public class UserUpdateValidator implements ConstraintValidator<UserUpdateValid, UserUpdateDTO> {

  private final HttpServletRequest request;
  private final UserRepository repository;

  @Override
  public boolean isValid(UserUpdateDTO dto, ConstraintValidatorContext context) {

	List<FieldMessage> list = new ArrayList<>();

	var uriVars = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
	var userId = UUID.fromString(uriVars.get("id"));

	var user = repository.findByEmail(dto.getEmail());
	if(user.isPresent() && user.get().getId().equals(userId)) {
	  list.add(new FieldMessage("Email", "Email já existe"));
	}

	for(FieldMessage e : list) {
	  context.disableDefaultConstraintViolation();
	  context.buildConstraintViolationWithTemplate(e.getMessage())
			 .addPropertyNode(e.getFieldName())
			 .addConstraintViolation();
	}
	return list.isEmpty();
  }

}
