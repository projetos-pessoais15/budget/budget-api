package br.com.budget.models;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Getter
@Setter
public class FieldError {

    private String field;
    private String errorCode;

}
