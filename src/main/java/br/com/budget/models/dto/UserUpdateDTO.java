package br.com.budget.models.dto;

import br.com.budget.models.validations.user.validation.UserUpdateValid;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@UserUpdateValid
public class UserUpdateDTO extends UserDTO { }

