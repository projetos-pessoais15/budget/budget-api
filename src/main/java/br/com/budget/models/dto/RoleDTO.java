package br.com.budget.models.dto;

import br.com.budget.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO implements Serializable {

  @Setter
  private Long id;
  @Setter
  private String authority;

  public RoleDTO(Role role) {
	this.id        = role.getId();
	this.authority = role.getAuthority();
  }

}

