package br.com.budget.controllers;

import br.com.budget.models.Response;
import br.com.budget.models.dto.AdditionalIncomeDTO;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.services.AdditionalIncomeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/additional-income")
@RequiredArgsConstructor
public class AdditionalIncomeController {

    private final AdditionalIncomeService additionalIncomeService;

    @GetMapping
    public ResponseEntity<Page<AdditionalIncomeDTO>> findAll(Pageable pageable) {
        var additionalIncomeList = additionalIncomeService.findAll(pageable);
        return ResponseEntity.ok()
                             .body(additionalIncomeList);
    }

    @GetMapping("/{name}")
    public ResponseEntity<List<AdditionalIncomeDTO>> findByName(@PathVariable("name") String name) {
        var additionalIncomeList = additionalIncomeService.findByName(name);
        return ResponseEntity.ok().body(additionalIncomeList);
    }

    @PostMapping
    public ResponseEntity<Response> save(@RequestBody AdditionalIncomeDTO additionalIncomeDTO) {
        var newAdditionalIncomeDTO = additionalIncomeService.save(additionalIncomeDTO);
        var uri = ServletUriComponentsBuilder.fromCurrentRequest()
                                             .path("/{id}")
                                             .buildAndExpand(newAdditionalIncomeDTO.getId())
                                             .toUri();
        return ResponseEntity.created(uri)
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.CREATED.value())
                                           .status(HttpStatus.CREATED)
                                           .message("URI - " + uri)
                                           .data(Map.of("new additional income: ", newAdditionalIncomeDTO))
                                           .build());
    }

    @PutMapping
    public ResponseEntity<Response> update(@RequestBody AdditionalIncomeDTO additionalIncomeDTO) {
        var updatedAdditionalIncomeDTO = additionalIncomeService.update(additionalIncomeDTO);
        return ResponseEntity.ok()
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.OK.value())
                                           .status(HttpStatus.OK)
                                           .data(Map.of("Additional Income updated: ", updatedAdditionalIncomeDTO))
                                           .build());
    }

    @DeleteMapping
    public ResponseEntity<Response> deleteById(@RequestBody EntityIdDTO entityIdDTO) {
        additionalIncomeService.deleteById(entityIdDTO);
        return ResponseEntity.ok()
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.OK.value())
                                           .status(HttpStatus.OK)
                                           .data(Map.of("Additional Income deleted: ", entityIdDTO.getId()))
                                           .build());
    }
}
