package br.com.budget.controllers;

import br.com.budget.enums.SpentType;
import br.com.budget.models.Response;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.models.dto.SpendingDTO;
import br.com.budget.services.SpendingService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
@RequestMapping("/spending")
@RequiredArgsConstructor
public class SpendingController {

    private final SpendingService spendingService;

    @GetMapping
    public ResponseEntity<Page<SpendingDTO>> findAll(Pageable pageable) {
        var pageSpendingDTO = spendingService.findAll(pageable);
        return ResponseEntity.ok()
                             .body(pageSpendingDTO);
    }

    @GetMapping("/date/{date}")
    public ResponseEntity<Page<SpendingDTO>> findByDate(Pageable pageable, @PathVariable("date") LocalDate date) {
        var pageSpendingDTO = spendingService.findByDate(pageable, date);
        return ResponseEntity.ok()
                             .body(pageSpendingDTO);
    }

    @GetMapping("/total")
    public ResponseEntity<Response> getTotal(@RequestParam("dateInitial") String dateInitial,
                                             @RequestParam("dateFinal") String dateFinal) {

        final var initialDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateInitial));
        final var finalDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateFinal));

        final var total = spendingService.getTotal(initialDate, finalDate);
        return ResponseEntity.ok()
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.OK.value())
                                           .status(HttpStatus.OK)
                                           .data(Map.of("Total: ", total))
                                           .build());
    }

    @GetMapping("/get-total-expense-per-month")
    public ResponseEntity<Response> getTotalExpensePerMonth(@RequestParam("dateInitial") String dateInitial,
                                                            @RequestParam("dateFinal") String dateFinal) {

        final var initialDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateInitial));
        final var finalDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateFinal));

        final var total = spendingService.getTotalExpensePerMonth(initialDate, finalDate);
        return ResponseEntity.ok().body(Response.builder()
                                                .timeStamp(LocalDateTime.now())
                                                .statusCode(HttpStatus.OK.value())
                                                .status(HttpStatus.OK)
                                                .data(Map.of("Total: ", total))
                                                .build());
    }

    @GetMapping("/get-grand-total-per-month")
    public ResponseEntity<Response> getTotalExpensePerMonth(@RequestParam("dateInitial") String dateInitial,
                                                            @RequestParam("dateFinal") String dateFinal,
                                                            @RequestParam("spentType") SpentType spentType) {

        final var initialDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateInitial));
        final var finalDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateFinal));

        final var total = spendingService.getGrandTotal(initialDate, finalDate, spentType);
        return ResponseEntity.ok().body(Response.builder()
                                                .timeStamp(LocalDateTime.now())
                                                .statusCode(HttpStatus.OK.value())
                                                .status(HttpStatus.OK)
                                                .data(Map.of("Grand Total: ", total))
                                                .build());
    }

    // TODO implementar controller da regra 50 30 20

    @PostMapping
    public ResponseEntity<Response> save(@RequestBody SpendingDTO spendingDTO) {
        spendingDTO = spendingService.save(spendingDTO);
        var uri = ServletUriComponentsBuilder.fromCurrentRequest()
                                             .path("/{id}")
                                             .buildAndExpand(spendingDTO.getId())
                                             .toUri();
        return ResponseEntity.created(uri)
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.CREATED.value())
                                           .status(HttpStatus.CREATED)
                                           .data(Map.of("New Spending: ", spendingDTO))
                                           .build());
    }

    @PutMapping
    public ResponseEntity<Response> update(@RequestBody SpendingDTO spendingDTO) {
        spendingDTO = spendingService.update(spendingDTO);
        return ResponseEntity.ok()
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.OK.value())
                                           .status(HttpStatus.OK)
                                           .data(Map.of("Spending updated: ", spendingDTO))
                                           .build());
    }

    @DeleteMapping
    public ResponseEntity<Response> delete(@RequestBody EntityIdDTO entityIdDTO) {
        spendingService.delete(entityIdDTO);
        return ResponseEntity.ok()
                             .body(Response.builder()
                                           .timeStamp(LocalDateTime.now())
                                           .statusCode(HttpStatus.OK.value())
                                           .status(HttpStatus.OK)
                                           .data(Map.of("Spending deleted: ", entityIdDTO.getId()))
                                           .build());
    }
}
