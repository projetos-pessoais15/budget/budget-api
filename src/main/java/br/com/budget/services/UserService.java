package br.com.budget.services;

import br.com.budget.entities.Role;
import br.com.budget.entities.User;
import br.com.budget.models.dto.UserDTO;
import br.com.budget.models.dto.UserInsertDTO;
import br.com.budget.models.dto.UserUpdateDTO;
import br.com.budget.repositories.RoleRepository;
import br.com.budget.repositories.UserRepository;
import br.com.budget.services.exceptions.DatabaseException;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.UUID;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Transactional(readOnly = true)
    public Page<UserDTO> findAll(Pageable pageable) {
        logger.info("find all users");
        var list = userRepository.findAll(pageable);
        return list.map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public UserDTO findById(final UUID id) {
        var user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
        return new UserDTO(user);
    }

    @Transactional(readOnly = true)
    public User getUserDetails() {
        var email = (String) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Transactional
    public UserDTO save(final UserInsertDTO userInsertDTO) {
        var user = new User();
        final var roles = new HashSet<Role>();
        BeanUtils.copyProperties(userInsertDTO, user);
        if(!userInsertDTO.getRoles().isEmpty()) {
            userInsertDTO.getRoles().forEach((role) -> {
                roles.add(roleRepository.findById(role.getId()).orElseThrow(() -> new ResourceNotFoundException("Role not found")));
            });
            user.setRoles(roles);
        }
        var password = passwordEncoder.encode(userInsertDTO.getPassword());
        user.setPassword(password);
        user = userRepository.save(user);
        return new UserDTO(user);
    }

    @Transactional
    public UserDTO update(final UserUpdateDTO userUpdateDTO) {
        var userFromBD = userRepository.findById(userUpdateDTO.getId())
                .orElseThrow(
                        () -> new ResourceNotFoundException("Id not found " + userUpdateDTO.getId()));
        BeanUtils.copyProperties(userUpdateDTO, userFromBD);
        var user = userRepository.save(userFromBD);
        return new UserDTO(user);
    }

    public void delete(final UUID id) {
        try {
            userRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new ResourceNotFoundException("Id not found");
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var user = userRepository.findByEmail(email);
        if (user.isEmpty()) {
            logger.error("User not found: {}", email);
            throw new UsernameNotFoundException("Could not find User with email = " + email);
        }
        logger.info("User found: {}", email);
        return user.get();
    }

}
