package br.com.budget.services.exceptions;

public class LoginInvalidException extends RuntimeException{

    public LoginInvalidException(String message) {
        super(message);
    }
}
