package br.com.budget.entities;

import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@ActiveProfiles("test")
class RevenueTests {

    private Revenue revenue;
    private String name;

    @BeforeEach
    void setup() {
        name = LocalDate.now().getMonth().toString() + "-" + LocalDate.now().getYear();
        revenue = Factory.createRevenue();
    }

    @Test
    void shouldCreateANewRevenue() {
        var newRevenue = new Revenue();
        Assertions.assertNotNull(newRevenue);
        Assertions.assertEquals(Revenue.class, newRevenue.getClass());
    }

    @Test
    void shouldCheckRevenue() {

        Assertions.assertEquals(UUID.class, revenue.getId().getClass());
        Assertions.assertEquals(LocalDate.now(), revenue.getDate());
        Assertions.assertEquals(name, revenue.getName());
        Assertions.assertEquals(BigDecimal.valueOf(2500.00), revenue.getWage());
        Assertions.assertEquals(Timestamp.class, revenue.getCreatedAt().getClass());
        Assertions.assertEquals(Timestamp.class, revenue.getUpdatedAt().getClass());
        Assertions.assertFalse(revenue.getAdditionalIncoming().isEmpty());

    }

    @Test
    void shouldUpdateRevenue() {
        var newRevenue = new Revenue();
        newRevenue.setId(UUID.randomUUID());
        newRevenue.setDate(LocalDate.now());
        newRevenue.setName("Nome Atualizado");
        newRevenue.setWage(BigDecimal.valueOf(3000.00));

        Assertions.assertNotEquals(newRevenue, revenue);
    }

    @Test
    void shouldUpdateNameAnPrePersist(){
        var revenue = Factory.createRevenue();

        revenue.prePersist();
        Assertions.assertNotNull(revenue.getName());
    }
}
