package br.com.budget.entities;

import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Timestamp;

@ActiveProfiles("test")
class RoleTests {

    @Test
    void shouldCreateANewRole() {
        var newRole = Factory.createRole(1L, "ADMIN");

        Assertions.assertNotNull(newRole);
        Assertions.assertEquals(1L, newRole.getId());
        Assertions.assertEquals("ADMIN", newRole.getAuthority());

        Assertions.assertEquals(Timestamp.class, newRole.getCreatedAt().getClass());
        Assertions.assertEquals(Timestamp.class, newRole.getUpdatedAt().getClass());
    }

    @Test
    void shouldUpdateRole() {
        var role = Factory.createRole(4L, "USUARIO");
        var newRole = new Role();

        Assertions.assertNotEquals(role.getId(), newRole.getId());
        Assertions.assertNotEquals(role.getAuthority(), newRole.getAuthority());
    }

    @Test
    void shouldCreateNewRole() {
        var newRole = new Role();
        Assertions.assertEquals(Role.class, newRole.getClass());
    }
}
