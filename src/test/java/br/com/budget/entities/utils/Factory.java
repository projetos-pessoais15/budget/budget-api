package br.com.budget.entities.utils;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.Revenue;
import br.com.budget.entities.Role;
import br.com.budget.entities.Spending;
import br.com.budget.entities.User;
import br.com.budget.enums.SpentType;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class Factory {

    public static Role createRole(long id, String authority) {
        return Role.builder()
                .id(id)
                .authority(authority)
                .createdAt(Timestamp.from(new Date().toInstant()))
                .updatedAt(Timestamp.from(new Date().toInstant()))
                .build();
    }

    public static User createUser() {
        final var roles = new HashSet<Role>();
        roles.add(createRole(1L, "ADMIN"));
        return User.builder()
                .id(UUID.randomUUID())
                .firstName("User")
                .lastName("Test")
                .email("user@gmail.com")
                .password("12345")
                .roles(roles)
                .createdAt(Timestamp.from(new Date().toInstant()))
                .updatedAt(Timestamp.from(new Date().toInstant()))
                .isAccountNonExpired(Boolean.TRUE)
                .isAccountNonLocked(Boolean.TRUE)
                .isCredentialsNonExpired(Boolean.TRUE)
                .isEnabled(Boolean.TRUE)
                .build();
    }

    public static AdditionalIncome createAdditionalIncome() {
        return AdditionalIncome.builder()
                .id(UUID.randomUUID())
                .name("FGTS-" + LocalDate.now())
                .value(new BigDecimal("500.00"))
                .createdAt(Timestamp.from(new Date().toInstant()))
                .updatedAt(Timestamp.from(new Date().toInstant()))
                .revenue(createRevenue())
                .build();
    }

    public static Revenue createRevenue() {
        var additionalIncomeList = List.of(new AdditionalIncome());
        return Revenue.builder()
                .id(UUID.randomUUID())
                .date(LocalDate.now())
                .name(LocalDate.now()
                        .getMonth()
                        .toString() + "-" + LocalDate.now()
                        .getYear())
                .wage(BigDecimal.valueOf(2500.00))
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .additionalIncoming(additionalIncomeList)
                .build();

    }

    public static Spending createSpending() {
        return Spending.builder()
                .id(UUID.randomUUID())
                .name("Nubank")
                .description("Dispesas com cartão de crédito")
                .value(new BigDecimal("200.00"))
                .date(LocalDate.now())
                .wasPaid(Boolean.TRUE)
                .spentType(SpentType.ESSENTIAL)
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .build();

    }


}
