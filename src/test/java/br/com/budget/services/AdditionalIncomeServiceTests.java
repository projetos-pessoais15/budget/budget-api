package br.com.budget.services;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.utils.Factory;
import br.com.budget.models.dto.AdditionalIncomeDTO;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.UUID;

@Transactional
@SpringBootTest
@ActiveProfiles("test")
class AdditionalIncomeServiceTests {

    private final AdditionalIncomeService additionalIncomeService;
    private EntityIdDTO entityIdDTO;
    private UUID nonExistingId;

    @Autowired
    public AdditionalIncomeServiceTests(AdditionalIncomeService additionalIncomeService) {
        this.additionalIncomeService = additionalIncomeService;
    }

    @BeforeEach
    void setup() {
        AdditionalIncome additionalIncome = Factory.createAdditionalIncome();
        AdditionalIncomeDTO additionalIncomeDTO = new AdditionalIncomeDTO(additionalIncome);
        additionalIncomeDTO = additionalIncomeService.save(additionalIncomeDTO);

        UUID existingId = additionalIncomeDTO.getId();
        entityIdDTO = new EntityIdDTO(existingId);
        nonExistingId = UUID.randomUUID();
    }

    @Test
    void findByNameShouldAListWhenNameExists() {
        final var list = additionalIncomeService.findByName("FGTS-"+ LocalDate.now());
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void saveShouldANewObject() {
        final var newAdditionalIncome = additionalIncomeService.save(new AdditionalIncomeDTO(Factory.createAdditionalIncome()));
        Assertions.assertNotNull(newAdditionalIncome);
    }

    @Test
    void findByIdShouldFindAnObjectWhenIdIsPresent() {
        final var dto = additionalIncomeService.findById(entityIdDTO);
        Assertions.assertNotNull(dto);
    }

    @Test
    void findByIdNotShouldFindAnObjectWhenIdIsNotPresent() {
        entityIdDTO = new EntityIdDTO(nonExistingId);
        Assertions.assertThrows(ResourceNotFoundException.class, () -> additionalIncomeService.findById(entityIdDTO));
    }

}
